package apc

import (
	"fmt"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/soniah/gosnmp"
)

const (
	// OutletUndefined Returned on error response from device
	OutletUndefined int = -1

	// OutletOn is on / turn outlet on
	OutletOn int = 1

	// OutletOff is off / turn outlet off
	OutletOff int = 2

	// OutletReboot is rebooting / reboot outlet
	OutletReboot int = 3

	// GetOutletOIDPrefix  OID prefix used to get outlet state
	GetOutletOIDPrefix string = ".1.3.6.1.4.1.318.1.1.12.3.5.1.1.4"

	// SetOutletOIDPrefix  OID prefix used to set outlet state
	SetOutletOIDPrefix string = ".1.3.6.1.4.1.318.1.1.12.3.3.1.1.4"

	//
)

var (

	// Version of this software
	Version string
	// global lock for access lock map
	mtx sync.Mutex

	// a map of locks one for each target address
	lkm map[string]*sync.Mutex = make(map[string]*sync.Mutex)

	// Retries Number of times to set and check
	Retries = 5
)

// On turns an outlet on.
func On(addr string, outlet int) error {

	return mod(addr, outlet, OutletOn)

}

// Off turns an outlet off.
func Off(addr string, outlet int) error {

	return mod(addr, outlet, OutletOff)

}

// Cycle power cycles an outlet.
func Cycle(addr string, outlet int) error {
	fields := log.Fields{
		"address": addr,
		"outlet":  outlet,
	}

	err := mod(addr, outlet, OutletOff)
	if err != nil {
		log.WithFields(fields).Errorf("failed to off: %v", err)
		return err
	}

	time.Sleep(7 * time.Second)

	err = mod(addr, outlet, OutletOn)
	if err != nil {
		log.WithFields(fields).Errorf("failed to off: %v", err)
		return err
	}

	return nil

}

// State retrieves the current state of an outlet
func State(addr string, outlet int) (int, error) {

	g := newSNMPClient(addr)
	err := g.Connect()
	if err != nil {
		return OutletUndefined, err
	}
	defer g.Conn.Close()

	oid := fmt.Sprintf("%s.%d", GetOutletOIDPrefix, outlet)
	result, err := g.Get([]string{oid})
	if err != nil {
		return OutletUndefined, err
	}

	for _, x := range result.Variables {
		switch x.Type {
		case gosnmp.Integer:
			return x.Value.(int), nil
		}
	}

	return OutletUndefined, fmt.Errorf("not found")

}

func mod(addr string, outlet int, action int) error {

	// get exclusive access to the lockmap
	mtx.Lock()

	// get the lock for this target address
	lk, ok := lkm[addr]
	if !ok {

		lk = &sync.Mutex{}
		lkm[addr] = lk

	}

	// get exclusive access to this target address
	lk.Lock()

	// release exclusive access to the lockmap
	mtx.Unlock()
	defer lk.Unlock()

	status := -1
	retry := Retries
	for status != action && retry > 0 {
		g := newSNMPClient(addr)
		err := g.Connect()
		if err != nil {
			return err
		}
		defer g.Conn.Close()

		pdu := gosnmp.SnmpPDU{
			Name:  fmt.Sprintf("%s.%d", SetOutletOIDPrefix, outlet),
			Type:  gosnmp.Integer,
			Value: action,
		}

		pkt, err := g.Set([]gosnmp.SnmpPDU{pdu})
		if err != nil {
			log.Error(err)
			continue
		}
		if pkt.Error != 0 {
			switch pkt.Error {
			case gosnmp.TooBig:
				log.Errorf("too big")
			case gosnmp.NoSuchName:
				log.Errorf("no such name")
			case gosnmp.BadValue:
				log.Errorf("bad value")
			case gosnmp.ReadOnly:
				log.Errorf("read only")
			case gosnmp.GenErr:
				log.Errorf("general error")
			case gosnmp.NoAccess:
				log.Errorf("no access")
			case gosnmp.WrongType:
				log.Errorf("wrong type")
			case gosnmp.WrongLength:
				log.Errorf("wrong length")
			case gosnmp.WrongEncoding:
				log.Errorf("wrong encoding")
			case gosnmp.WrongValue:
				log.Errorf("wrong value")
			case gosnmp.NoCreation:
				log.Errorf("no creation")
			case gosnmp.InconsistentValue:
				log.Errorf("inconsistent value")
			case gosnmp.ResourceUnavailable:
				log.Errorf("resource unavailable")
			case gosnmp.CommitFailed:
				log.Errorf("commit failed")
			case gosnmp.UndoFailed:
				log.Errorf("undo failed")
			case gosnmp.AuthorizationError:
				log.Errorf("authorization failed")
			case gosnmp.NotWritable:
				log.Errorf("not writable")
			case gosnmp.InconsistentName:
				log.Errorf("inconsistent name")
			}
			continue
		}

		// sleep between check, both giving outlet enough
		// time to switch state, but also to have a backoff
		// timer between each loop
		time.Sleep(500 * time.Millisecond)

		// check that the switch enacted our message
		status, err = State(addr, outlet)
		if err != nil {
			log.Error(err)
			continue
		}
		state := ""
		switch status {
		case OutletUndefined:
			state = "undefined"
		case OutletOff:
			state = "off"
		case OutletOn:
			state = "on"
		case OutletReboot:
			state = "cycling"
		}

		log.Infof("apc %s (%d): %s", addr, outlet, state)

		// decrement our retry counter
		retry--
	}
	// we've failed
	if status != action && retry > 0 {
		return fmt.Errorf("failed to set apc")
	}
	return nil

}

func newSNMPClient(addr string) *gosnmp.GoSNMP {

	return &gosnmp.GoSNMP{
		Port:               161,
		Transport:          "udp",
		Community:          "private",
		Version:            gosnmp.Version1,
		Timeout:            time.Duration(2) * time.Second,
		Retries:            3,
		ExponentialTimeout: true,
		MaxOids:            gosnmp.MaxOids,
		Target:             addr,
	}

}
