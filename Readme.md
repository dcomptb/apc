# APC

An SNMP based APC PDU power controller. May be useful as utility or library.

## Usage

```shell
Usage:
  apc [command]

Available Commands:
  cycle       cycle outlet
  help        Help about any command
  off         turn outlet off
  on          turn outlet on
  status      get current outlet status

Flags:
  -h, --help   help for apc

Use "apc [command] --help" for more information about a command.

```
