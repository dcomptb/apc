package main

import (
	"log"
	"strconv"

	"github.com/spf13/cobra"

	"gitlab.com/dcomptb/apc/pkg"
)

func main() {
	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "apc",
		Short: "APC PDU outlet control utility",
	}

	on := &cobra.Command{
		Use:   "on <pdu> <outlet>",
		Short: "turn outlet on",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			outlet, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal(err)
			}
			err = apc.On(args[0], outlet)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	root.AddCommand(on)

	off := &cobra.Command{
		Use:   "off <pdu> <outlet>",
		Short: "turn outlet off",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			outlet, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal(err)
			}
			err = apc.Off(args[0], outlet)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	root.AddCommand(off)

	cycle := &cobra.Command{
		Use:   "cycle <pdu> <outlet>",
		Short: "cycle outlet",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			outlet, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal(err)
			}
			err = apc.Cycle(args[0], outlet)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	root.AddCommand(cycle)

	status := &cobra.Command{
		Use:   "status <pdu> <outlet>",
		Short: "get current outlet status",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			outlet, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal(err)
			}
			state, err := apc.State(args[0], outlet)
			if err != nil {
				log.Fatal(err)
			}
			switch state {
			case apc.OutletUndefined:
				log.Println("undefined")
			case apc.OutletOff:
				log.Println("off")
			case apc.OutletOn:
				log.Println("on")
			case apc.OutletReboot:
				log.Println("cycling")
			}
		},
	}
	root.AddCommand(status)

	root.Execute()
}
