all: build/apc

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/dcomptb/apc/pkg.Version=$(VERSION)"

build/apc: cmd/apc/main.go pkg/*.go | build
	$(go-build)

build:
	$(QUIET) mkdir -p build

.PHONY: clean
clean:
	$(QUIET) rm -rf build

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=${LDFLAGS} -o $@ $(dir $<)/*
endef
